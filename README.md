Many Sitecore Implementation includes different type of items: Page Items but also Data Items. As part of sitecore best practices, we will use Datasources for rendering quite extensively. This means that every pages on our website are composed of one main Item (Page Item itself) plus all the Datasources Items which can be defined in different location. Although we can use the workflow history to view the item history, it is quite troublesome to view all the page elements history in one place. This module is intended to help content editor to view the page elements, their location on the tree as well as their history (Version and Workflow history).

This module is intended as a sample to show:
* How to add a new Tab on the content Editor
* How to Use Sitecore API as Simple IoC
* How to use a custom SPEAK App. For the purpose of the sample code, the SPEAK app is only a one custom rendering which could be easily split into several smaller rendering if require. However, making it a full SPEAK App was not the main purpose here.
