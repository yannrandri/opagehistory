﻿define(["sitecore"], function (Sitecore) {
    var model = Sitecore.Definitions.Models.ControlModel.extend({
        initialize: function (options) {
            this._super();
            app = this;
            app.set("pagehistory", '');
            app.GetPageHistory(app);
        },
        GetPageHistory: function (app) {
            jQuery.ajax({
                type: "GET",
                dataType: "json",
                url: "/OPageHistory/OPageHistory/GetCurrentItem?id=" + this.GetQueryStringParameter("id") + "&database=" + this.GetQueryStringParameter("database") + "&language=" + this.GetQueryStringParameter("language") + "&version=" + this.GetQueryStringParameter("version"),
                cache: false,
                success: function (data) {
                    app.set("pagehistory", data);
                },
                error: function () {
                    console.log("There was an error in GetPageHistory() function!");
                }
            });
        },

        GetQueryStringParameter: function (name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    });

    var view = Sitecore.Definitions.Views.ControlView.extend({
        initialize: function (options) {
            this._super();
        },
        /* This function is bound to a <input> click event in the .cshtml */
        ToggleNode: function (element) {
            var val = $(element).val();
            $(element).val(val === "+" ? "-" : "+");
            this.ExpandParent(element);
        },

        ExpandParent: function (element) {
            $($(element).parent().parent().siblings()[0]).toggle("fast");
        },
        ToggleHistoryNode: function (element) {
            var val = $(element).text();
            $(element).html(val === "[+ Show version history]" ? "[- Hide version history]" : "[+ Show version history]");
            this.ExpandHistory(element);
        },

        ExpandHistory: function (element) {
            $($(element).parent().next()).toggle("fast");
        },

        ToggleVersionNode: function (element) {
            var val = $(element).text();
            $(element).html(val === "[+ Show all versions details]" ? "[- Hide versions details]" : "[+ Show all versions details]");
            this.ExpandPreviousVersions(element);
        },

        ExpandPreviousVersions: function (element) {
            $($(element).next()).toggle("fast");
        },
        ToggleReferrersNode: function (element) {
            var val = $(element).text();
            $(element).html(val === "[+ Show all Referrer items]" ? "[- Hide all Referrer items]" : "[+ Show all Referrer items]");
            this.ExpandReferrersVersions(element);
        },

        ExpandReferrersVersions: function (element) {
            $($(element).next()).toggle("fast");
        },
        GotoDatasourceItem: function (data, event) {
            parent.scForm.postRequest("", "", "", "item:load(id=" + data.DatasourceID + ")");
            return false;
        },
        GotoChildItem: function (data, event) {
            parent.scForm.postRequest("", "", "", "item:load(id=" + data.ItemId + ")");
            return false;
        }
    });

    Sitecore.Factories.createComponent("ItemHistory", model, view, ".sc-ItemHistory");
});
