﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Pipelines;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Http;

namespace OPageHistory.Web.Areas.OPageHistory.Pipelines
{
    public class RegisterOPageHistory
    {
        public virtual void Process(PipelineArgs args)
        {
            this.DoProcess();
        }

        protected void DoProcess()
        {
            try
            {
                List<RouteBase> list = Enumerable.ToList<RouteBase>(Enumerable.Where<RouteBase>((IEnumerable<RouteBase>)RouteTable.Routes, (Func<RouteBase, bool>)(route =>
                {
                    Route route1 = route as Route;
                    if (route1 != null && route1.Defaults != null && route1.Defaults.ContainsKey("area"))
                        return string.Equals(route1.Defaults["area"].ToString(), "OPageHistory", StringComparison.Ordinal);
                    return false;
                })));
                if (!Enumerable.Any<RouteBase>((IEnumerable<RouteBase>)list))
                {
                    new OPageHistoryAreaRegistration().RegisterArea(new AreaRegistrationContext("OPageHistory", RouteTable.Routes));
                }
                else
                {
                    foreach (Route route in list)
                        route.DataTokens["UseNamespaceFallback"] = (object)true;
                }
            }
            catch (Exception ex)
            {
                // Sitecore Log or specific?
            }
        }
    }
}
