﻿using Sitecore.Shell.Feeds.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OPageHistory.Business.Services;
using OPageHistory.Business.IoC;
using OPageHistory.Business.Interfaces;

namespace OPageHistory.Web.Areas.OPageHistory.Controllers
{
    public class OPageHistoryController : System.Web.Mvc.Controller
    {
        private IPageHistoryService pageHistoryService = TypeResolver.Resolve<IPageHistoryService>("PageHistoryService");

        /// <summary>
        /// get the full item history information including the renderings datasources
        /// </summary>
        /// <param name="id"></param>
        /// <param name="database"></param>
        /// <param name="language"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public ActionResult GetCurrentItem(string id, string database, string language, string version)
        {
            return Json(pageHistoryService.GetPageHistory(id, database, language, version), JsonRequestBehavior.AllowGet);

        }
    }
}
