﻿using System.Web.Mvc;

namespace OPageHistory.Web.Areas.OPageHistory
{
    public class OPageHistoryAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "OPageHistory";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "OPageHistory_default",
                "OPageHistory/{controller}/{action}/{id}",
                (object)new { area = "OPageHistory", action = "Index", id = UrlParameter.Optional },
                (object)new { 
                    controller = ".*OPageHistory"
                }
            );
        }
    }
}