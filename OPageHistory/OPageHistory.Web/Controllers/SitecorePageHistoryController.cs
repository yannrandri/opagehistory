﻿using Sitecore.Shell.Feeds.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OPageHistory.Business.Services;

namespace OPageHistory.Web.Controllers
{
    public class SitecorePageHistoryController : System.Web.Mvc.Controller
    {
        /// <summary>
        /// get the full item history information including the renderings datasources
        /// </summary>
        /// <param name="id"></param>
        /// <param name="database"></param>
        /// <param name="language"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public ActionResult GetCurrentItem(string id, string database, string language, string version)
        {
            PageHistoryService service = new PageHistoryService();
            return Json(service.GetPageHistory(id, database, language, version), JsonRequestBehavior.AllowGet);

        }
    }
}