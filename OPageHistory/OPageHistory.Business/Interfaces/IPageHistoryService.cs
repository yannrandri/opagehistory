﻿using OPageHistory.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPageHistory.Business.Interfaces
{
    public interface IPageHistoryService
    {
        ItemWrapper GetPageHistory(string itemId, string db, string language, string version);
    }
}
