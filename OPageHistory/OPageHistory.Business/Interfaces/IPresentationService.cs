﻿using OPageHistory.Data.Models;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Layouts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPageHistory.Business.Interfaces
{
    public interface IPresentationService
    {
        IEnumerable<RenderingReference> GetRenderingsReferences(Item currentItem, DeviceItem deviceItem);
        List<DevicePresentation> GetRenderingDatasources(Item item);
        IEnumerable<RenderingWrapperItem> GetRenderingDatasources(Item item, DeviceItem device);
        Sitecore.Data.Items.DeviceItem GetDeviceItem(string deviceName, Database db);
        Sitecore.Data.Items.DeviceItem GetDeviceItem(Item deviceItem);
    }
}
