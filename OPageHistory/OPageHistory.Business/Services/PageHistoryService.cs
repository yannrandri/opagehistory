﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPageHistory.Data.Models;
using OPageHistory.Business.Interfaces;
using OPageHistory.Business.IoC;

namespace OPageHistory.Business.Services
{
    public class PageHistoryService : IPageHistoryService
    {
        private IPresentationService presentationService = TypeResolver.Resolve<IPresentationService>("PresentationService");

        /// <summary>
        /// Get the Page Item History with the different datasources from the presentation
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="db"></param>
        /// <param name="language"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public ItemWrapper GetPageHistory(string itemId, string db, string language, string version)
        {
            if(presentationService ==null)
                return null;

            Sitecore.Data.ID id;
            if (!Sitecore.Data.ID.TryParse(itemId, out id))
                return null;

            Sitecore.Data.Database database = Sitecore.Configuration.Factory.GetDatabase(db);
            if (database == null)
                return null;

            Sitecore.Globalization.Language lang;
            if (!Sitecore.Globalization.Language.TryParse(language, out lang))
                return null;

            Sitecore.Data.Version itemVersion;
            if (!Sitecore.Data.Version.TryParse(version, out itemVersion))
                return null;

            var item = database.GetItem(itemId, lang, itemVersion);

            if (item == null)
                return null;

            // Get the History for the item and populate the Renderings Datasource
            var history = new ItemWrapper(item,false,true);
            history.Components = presentationService.GetRenderingDatasources(item);

            return history;
        }
    }
}
