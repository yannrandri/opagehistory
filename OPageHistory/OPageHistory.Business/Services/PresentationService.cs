﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Layouts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPageHistory.Data.Models;
using OPageHistory.Business.Interfaces;

namespace OPageHistory.Business.Services
{
    public class PresentationService: IPresentationService
    {
        /// <summary>
        /// Return the settings including datasource ID
        /// For current item and specific device
        /// </summary>
        /// <param name="currentItem"></param>
        /// <param name="deviceItem"></param>
        /// <returns></returns>
        public IEnumerable<RenderingReference> GetRenderingsReferences(Item currentItem, DeviceItem deviceItem)
        {
            Sitecore.Data.Fields.LayoutField layoutField = new Sitecore.Data.Fields.LayoutField(currentItem);

            if (deviceItem == null)
                return new List<RenderingReference>();

            Sitecore.Layouts.RenderingReference[] renderings = layoutField.GetReferences(deviceItem);
            var filteredRenderingList = new List<RenderingReference>();

            if (renderings == null)
                return filteredRenderingList;

            foreach (var rendering in renderings)
            {
                filteredRenderingList.Add(rendering);
            }

            return filteredRenderingList;
        }

        /// <summary>
        /// Get the full list of renderings for all devices. the key of the dictionary is the device item name
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public List<DevicePresentation> GetRenderingDatasources(Item item)
        {
            var presentationRenderings = new List<DevicePresentation>();

            var devicesItems = item.Database.GetItem(OPageHistory.Common.Constants.ItemIds.Devices);
            if (devicesItems != null && devicesItems.HasChildren)
            {
                foreach (var device in devicesItems.Children)
                {
                    var deviceItem = (Sitecore.Data.Items.Item)device;
                    var scDevice = GetDeviceItem(deviceItem);
                    presentationRenderings.Add(new DevicePresentation() { DeviceName = deviceItem.Name, Renderings = GetRenderingDatasources(item, scDevice) });
                }
            }

            return presentationRenderings;
        }

        /// <summary>
        /// Get the Datasources for all renderings inside a device
        /// </summary>
        /// <param name="item"></param>
        /// <param name="device"></param>
        /// <returns></returns>
        public IEnumerable<RenderingWrapperItem> GetRenderingDatasources(Item item, DeviceItem device)
        {
            var renderingDatasources = new List<RenderingWrapperItem>();

            IEnumerable<RenderingReference> renderingsSettings = this.GetRenderingsReferences(item, device);
            foreach (var renderingSettings in renderingsSettings)
            {
                if (renderingSettings.Settings.DataSource != null)
                {
                    Sitecore.Data.Items.Item datasourceItem;
                    if (Sitecore.Data.ID.IsID(renderingSettings.Settings.DataSource))
                    {
                        datasourceItem = item.Database.GetItem(new Sitecore.Data.ID(renderingSettings.Settings.DataSource));
                    }
                    else
                    {
                        datasourceItem = item.Database.GetItem(renderingSettings.Settings.DataSource);
                    }

                    if (datasourceItem == null)
                        continue;

                    renderingDatasources.Add(new RenderingWrapperItem(renderingSettings.RenderingItem,datasourceItem));
                }
            }

            return renderingDatasources;
        }

        /// <summary>
        /// Get the Device from the context database and the Device name
        /// </summary>
        /// <param name="deviceName"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public Sitecore.Data.Items.DeviceItem GetDeviceItem(string deviceName, Database db)
        {
            if (Sitecore.Data.ID.IsID(deviceName))
            {
                return db.Resources.Devices.GetAll().Where(d => d.ID.Guid == new Guid(deviceName)).First();
            }
            else
            {
                return db.Resources.Devices.GetAll().Where(d => d.Name.ToLower() == deviceName.ToLower()).First();
            }
        }

        /// <summary>
        /// Get the Device from the context database and the Device name
        /// </summary>
        /// <param name="deviceName"></param>
        /// <returns></returns>
        public Sitecore.Data.Items.DeviceItem GetDeviceItem(Item deviceItem)
        {
            return deviceItem.Database.Resources.Devices.GetAll().Where(d => d.ID == deviceItem.ID).First();
        }
    }
}
