﻿using OPageHistory.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPageHistory.Data.Comparers
{
    public class ItemWrapperComparer : IEqualityComparer<ItemWrapper>
    {
        /// <summary>
        /// XXX - This may not be used right now but it ha sbeen created for the next step
        /// </summary>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <returns></returns>
        public bool Equals(ItemWrapper item1, ItemWrapper item2)
        {
            if (item1 == null && item2 == null)
                return true;

            else if (item1 == null | item2 == null)
                return false;

            else if (item1.ItemId == item2.ItemId)
                return true;

            else
                return false;
        }

        public int GetHashCode(ItemWrapper item)
        {
            return item.GetHashCode();
        }
    }
}