﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPageHistory.Data.Models
{
    public class ItemWrapper
    {
        public string ItemId { get; set; }
        public string ItemPath { get; set; }
        public string ItemVersion { get; set; }
        public string ItemDatabase { get; set; }
        public string ItemLanguage { get; set; }
        public string LastUpdated { get; set; }

        public List<VersionHistory> ItemVersionHistory { get; set; }
        public List<ItemWrapper> PreviousVersions { get; set; }
        public List<DevicePresentation> Components { get; set; }

        public List<ItemWrapper> Children { get; set; }

        /// <summary>
        /// Constructor from Item
        /// </summary>
        /// <param name="item"></param>
        /// <param name="includeChildren"></param>
        /// <param name="includePreviousVersions"></param>
        public ItemWrapper(Item item, bool includeChildren, bool includePreviousVersions)
        {
            if (item == null)
                return;

            ItemId = item.ID.ToString();
            ItemDatabase = item.Database.Name;
            ItemLanguage = item.Language.Name;
            ItemPath = item.Paths.ContentPath;
            ItemVersion = item.Version.Number.ToString();
            LastUpdated = string.Empty;
            if (item.Statistics.Updated != null)
                LastUpdated = item.Statistics.Updated.ToString("dd/MM/yyyy");
            ItemVersionHistory = new List<VersionHistory>();
            Children = new List<ItemWrapper>();
            PreviousVersions = new List<ItemWrapper>();

            // Components will be populated at the service level
            Components = new List<DevicePresentation>();

            GetHistory(item);

            if (item.HasChildren && includeChildren)
            {
                AddChildren(item);
            }

            if (includePreviousVersions)
            {
                GetAllVersions(item);
            }
        }

        /// <summary>
        /// GetAllVersions of the Item
        /// this is informative only and do not need to include children and all versions as we only want to retrieve the item specific version
        /// </summary>
        /// <param name="item"></param>
        private void GetAllVersions(Item item)
        {
            var allOlderVersions = item.Versions.GetOlderVersions();
            if (allOlderVersions.Any())
            {
                foreach (var v in allOlderVersions)
                {
                    PreviousVersions.Add(new ItemWrapper(v, false, false));
                }
            }
        }

        /// <summary>
        /// Populate the History property from the workflow
        /// </summary>
        /// <param name="item"></param>
        private void GetHistory(Item item)
        {
            if (item == null)
                return;

            var workflowProvider = (item.Database.WorkflowProvider as Sitecore.Workflows.Simple.WorkflowProvider);
            if (workflowProvider == null)
                return;

            if (workflowProvider.HistoryStore == null)
                return;

            var workflowEvents = workflowProvider.HistoryStore.GetHistory(item);
            foreach (var wEvent in workflowEvents)
            {
                var vHistory = new VersionHistory(wEvent, item.Database);
                if (vHistory != null)
                    ItemVersionHistory.Add(vHistory);
            }
        }

        /// <summary>
        /// This will ensure that all descendant of a Component Item are on the list of children
        /// </summary>
        /// <param name="item"></param>
        private void AddChildren(Item item)
        {
            if (item.HasChildren)
            {
                foreach (var child in item.Children)
                {
                    var childItem = (Item)child;
                    if (childItem != null)
                    {
                        // retrieve all children, we do not need to include the descendant as we are already recursing.
                        // however we do need to include the entire versioning
                        Children.Add(new ItemWrapper(childItem, false, true));
                        AddChildren(childItem);
                    }
                }
            }
        }
    }
}
