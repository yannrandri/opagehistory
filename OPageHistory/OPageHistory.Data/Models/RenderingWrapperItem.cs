﻿using OPageHistory.Data.Comparers;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPageHistory.Data.Models
{
    public class RenderingWrapperItem
    {
        public string RenderingName { get; set; }
        public ItemWrapper ComponentItem { get; set; }
        public List<ItemWrapper> RelatedItems { get; set; }

        /// <summary>
        /// Get the rendering information including the Datasource and the rendering itself
        /// </summary>
        /// <param name="renderingItem"></param>
        /// <param name="datasourceItem"></param>
        public RenderingWrapperItem(Sitecore.Data.Items.RenderingItem renderingItem, Item datasourceItem)
        {
            if (renderingItem == null || datasourceItem == null)
                return;

            RenderingName = renderingItem.Name;
            ComponentItem = new ItemWrapper(datasourceItem, true, true);
            GetReferrer(datasourceItem);
        }

        /// <summary>
        /// Get all item referring to this specific item
        /// </summary>
        /// <param name="item"></param>
        private void GetReferrer(Item item)
        {
            ItemLink[] source = Globals.LinkDatabase.GetItemReferrers(item, false);
            var grouppedSource = (
                from link in source
                where item.Database.Name.Equals(link.TargetDatabaseName, System.StringComparison.OrdinalIgnoreCase)
                select link).GroupBy(x => x.SourceItemID).ToArray<IGrouping<Sitecore.Data.ID, ItemLink>>();

            RelatedItems = (
                from link in grouppedSource
                select link.First().GetSourceItem() into relatedItem
                where relatedItem != null
                 && relatedItem.Paths.FullPath.ToLower() != item.Paths.FullPath.ToLower()
                 && relatedItem.Paths.FullPath.ToLower().Contains("sitecore/content/")
                select new ItemWrapper(relatedItem, false, false)).ToList<ItemWrapper>();
        }

    }
}
