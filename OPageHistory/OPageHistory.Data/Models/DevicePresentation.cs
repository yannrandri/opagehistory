﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPageHistory.Data.Models
{
    public class DevicePresentation
    {
        public string DeviceName { get; set; }
        public IEnumerable<RenderingWrapperItem> Renderings { get; set; }

    }
}
