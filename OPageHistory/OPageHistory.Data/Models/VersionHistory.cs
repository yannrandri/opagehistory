﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPageHistory.Data.Models
{
    public class VersionHistory
    {
        public string OldState { get; set; }
        public string NewState { get; set; }
        public string UserName { get; set; }
        public List<KeyValuePair<string,string>> Comments { get; set; }
        public string ChangeDate { get; set; }

        /// <summary>
        /// Get a workflow history event details 
        /// </summary>
        /// <param name="wEvent"></param>
        /// <param name="db"></param>
        public VersionHistory (Sitecore.Workflows.WorkflowEvent wEvent, Sitecore.Data.Database db)
        {
            if (wEvent == null)
                return;

            var oldStateItem = (Sitecore.Data.ID.IsID(wEvent.OldState))? db.GetItem(Sitecore.Data.ID.Parse(wEvent.OldState)) : null;
            OldState = (oldStateItem!=null) ? oldStateItem.DisplayName : string.Empty;

            var newStateItem = (Sitecore.Data.ID.IsID(wEvent.NewState)) ? db.GetItem(Sitecore.Data.ID.Parse(wEvent.NewState)) : null;
            NewState = (newStateItem != null) ? newStateItem.DisplayName : string.Empty;

            Comments = wEvent.CommentFields.ToList();
            UserName = wEvent.User;
            ChangeDate = wEvent.Date.ToString("dd/MM/yyyy");
        }
    }
}
